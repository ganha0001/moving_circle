.eqv 	KEY_CODE 0xFFFF0004  		# ASCII code from keyboard, 1 byte
.eqv 	KEY_READY 0xFFFF0000  		# =1 if has a new keycode ?

.text
main:
	li	$k0, KEY_CODE
	li	$k1, KEY_READY
	lw 	$t6, center		# set center of x coordinate of the 1st circle
	lw  	$t7, center          	# set center of y coordinate of the 1st circle
	lw  	$t8, center           	# x of 2nd 
	lw  	$t9, center           	# y of 2nd

	jal     draw_first_circle
 
WaitForKey:  
	lw	$t2, 0($k1)   		# $t1 = [$k1] = KEY_READY
	beq	$t2, $zero, WaitForKey 	# if $t1 == 0 then Polling
ReadKey:  
	lw	$t3, 0($k0)   		# $t0 = [$k0] = KEY_CODE
	beq	$t3, 97, goleft		# press d -> turn left
	beq	$t3, 100, goright	# press a -> turn right
	beq	$t3, 115, godown	# press s -> go down
	beq	$t3, 119, goup		# press w -> go up
	
goup:
	li	$t3, 119
	subi	$t7, $t7, 2		# move the circle
	jal	draw_first_circle	# show the circle
	jal	draw_second_circle	# hide the previous position  
	subi	$t9, $t9, 2		
	beq	$t7, 10, godown		# colide with the upper boundary
	lw	$t2, 0($k1)
	beq	$t2, $zero, goup	# colide with the lower boundary
	lw	$t3, 0($k0)
	beq	$t3, 115, godown	# press s -> go down
	beq	$t3, 97, goleft		# press a -> turn left
	beq	$t3, 100, goright	# press d -> turn right
	j	goup

godown:
	li	$t3, 115
	addi	$t7, $t7, 2		# move the circle
	jal	draw_first_circle	# show the circle
	jal	draw_second_circle	# hide the previous position
	addi	$t9, $t9, 2		
	beq	$t7, 502, goup		# colide with the upper boundary
	lw	$t2, 0($k1)
	beq	$t2, $zero, godown	# colide with the lower boundary	
	lw	$t3, 0($k0)
	beq	$t3, 119, goup		# press w -> go up
	beq	$t3, 97, goleft		# press a -> turn left
	beq	$t3, 100, goright	# press d -> turn right
	j	godown
     
goleft:
	li	$t3, 97
	subi	$t6, $t6, 2		# move the circle
	jal	draw_first_circle	# show the circle
	jal	draw_second_circle	# hide the previous position
	subi	$t8, $t8, 2
	beq	$t6, 10, goright	# colide with the left boundary
	lw	$t2, 0($k1)
	beq	$t2, $zero, goleft	# colide with the right boundary
	lw	$t3, 0($k0)
	beq	$t3, 119, goup		# press w -> go up
	beq	$t3, 115, godown	# press s -> go down
	beq	$t3, 100, goright	# press d -> turn right
	j	goleft
     
goright:
	li	$t3, 100
	addi	$t6, $t6, 2		# move the circle
	jal	draw_first_circle	# show the circle
	jal	draw_second_circle	# hide the previous position
	addi 	$t8,$t8, 2
	beq 	$t6, 502, goleft	# colide with the right boundary
	lw   	$t2, 0($k1)
	beq	$t2, $zero, goright	# colide with the left boundary
	lw  	$t3, 0($k0)
	beq	$t3, 119, goup		# press w -> go up
	beq 	$t3, 115, godown	# press s -> go down
	beq 	$t3, 97, goleft		# press a -> turn left
	j 	goright
     
draw_first_circle:
	subi	$sp, $sp, 4		
	sw	$ra, 0($sp)

	lw      $s0, radius		# x = radius
	li      $s1, 0                  # y = 0

    	# initialize: xchg = 1 - (2 * r)
	li      $s3, 1			# xchg = 1
	sll     $t0, $s0, 1		# get 2 * r
	sub     $s3, $s3, $t0		# xchg -= (2 * r)

	li	$s4, 1			# ychg = 1
	li	$s2, 0			# raderr = 0

first_circle_draw_loop:
	blt     $s0, $s1, first_circle_draw_done      	# x >= y? if no, fly (we're done)

	jal     first_circle_draw8

	addi    $s1, $s1, 1              # y += 1
	add     $s2, $s2, $s4            # raderr += ychg
	addi    $s4, $s4, 2              # ychg += 2

	sll     $t0, $s2, 1              # get 2 * raderr
	add     $t0, $t0, $s3            # get (2 * raderr) + xchg
	blez    $s2, first_circle_draw_loop          	# >0? if no, loop

	subi    $s0, $s0, 1              # x -= 1
	add     $s2, $s2, $s3            # raderr += xchg
	addi    $s3, $s3, 2              # xchg += 2
	j       first_circle_draw_loop

first_circle_draw_done:
	lw      $ra, 0($sp)
	addi    $sp, $sp, 4
	jr      $ra

draw_second_circle:
	subi    $sp, $sp, 4
	sw      $ra, 0($sp)

	lw      $s0, radius              # x = radius
	li      $s1, 0                   # y = 0

    # initialize: xchg = 1 - (2 * r)
	li      $s3, 1                   # xchg = 1
	sll     $t0, $s0, 1              # get 2 * r
	sub     $s3, $s3, $t0            # xchg -= (2 * r)

	li      $s4, 1                   # ychg = 1
	li      $s2, 0                   # raderr = 0

second_circle_draw_loop:
	blt     $s0, $s1, second_circle_draw_done      	# x >= y? if no, fly (we're done)

	jal     second_circle_draw8

	addi    $s1, $s1, 1              # y += 1
	add     $s2, $s2, $s4            # raderr += ychg
	addi    $s4, $s4, 2              # ychg += 2

	sll     $t0, $s2, 1              # get 2 * raderr
	add     $t0, $t0, $s3            # get (2 * raderr) + xchg
	blez    $s2, second_circle_draw_loop          # >0? if no, loop

	subi    $s0, $s0, 1              # x -= 1
	add     $s2, $s2, $s3            # raderr += xchg
	addi    $s3, $s3, 2              # xchg += 2
	j       second_circle_draw_loop

second_circle_draw_done:
	lw      $ra, 0($sp)
	addi    $sp, $sp, 4
	jr      $ra
	
# draw8 -- draw single point in all 8 octants
#
# arguments:
#   s0 -- X coord
#   s1 -- Y coord
#

first_circle_draw8:
	subi    $sp, $sp, 4
	sw      $ra, 0($sp)

    # draw [+x,+y]
	add     $a0, $t6, $s0
	add     $a1, $t7, $s1
	jal     first_circle_setpixel

    # draw [+y,+x]
	add     $a0, $t6, $s1
	add     $a1, $t7, $s0
	jal     first_circle_setpixel

    # draw [-x,+y]
	sub     $a0, $t6, $s0
	add     $a1, $t7, $s1
	jal     first_circle_setpixel

    # draw [-y,+x]
	add     $a0, $t6, $s1
	sub     $a1, $t7, $s0
	jal     first_circle_setpixel

    # draw [-x,-y]
	sub     $a0, $t6, $s0
	sub     $a1, $t7, $s1
	jal     first_circle_setpixel

    # draw [-y,-x]
	sub     $a0, $t6, $s1
	sub     $a1, $t7, $s0
	jal     first_circle_setpixel

    # draw [+x,-y]
	add     $a0, $t6, $s0
	sub     $a1, $t7, $s1
	jal     first_circle_setpixel

    # draw [+y,-x]
	sub     $a0, $t6, $s1
	add     $a1, $t7, $s0
	jal     first_circle_setpixel

	lw      $ra, 0($sp)
	addi    $sp, $sp, 4
	jr      $ra
    
second_circle_draw8:
	subi    $sp, $sp, 4
	sw      $ra, 0($sp)
    
    # draw [+x,+y]
	add     $a0, $t8, $s0
	add     $a1, $t9, $s1
	jal     second_cricle_setpixel

    # draw [+y,+x]
	add     $a0, $t8, $s1
	add     $a1, $t9, $s0
	jal     second_cricle_setpixel

    # draw [-x,+y]
	sub     $a0, $t8, $s0
	add     $a1, $t9, $s1
	jal     second_cricle_setpixel

    # draw [-y,+x]
	add     $a0, $t8, $s1
	sub     $a1, $t9, $s0
	jal     second_cricle_setpixel

    # draw [-x,-y]
	sub     $a0, $t8, $s0
	sub     $a1, $t9, $s1
	jal     second_cricle_setpixel

    # draw [-y,-x]
	sub     $a0, $t8, $s1
	sub     $a1, $t9, $s0
	jal     second_cricle_setpixel

    # draw [+x,-y]
	add     $a0, $t8, $s0
	sub     $a1, $t9, $s1
	jal     second_cricle_setpixel

    # draw [+y,-x]
	sub     $a0, $t8, $s1
	add     $a1, $t9, $s0
	jal     second_cricle_setpixel

	lw      $ra, 0($sp)
	addi    $sp, $sp, 4
	jr      $ra

# setpixel -- draw pixel on display
#
# arguments:
#   a0 -- X coord
#   a1 -- Y coord
#
# clobbers:
#   v0 -- bitmap offset/index
#   v1 -- bitmap address
# trace:
#   v0,a0

first_circle_setpixel:
	lw      $v0, default_width_height          # off = display width

	mul     $v0, $a1, $v0           # off = y * width
	add     $v0, $v0, $a0           # off += x
	sll     $v0, $v0, 2             # convert to offset

	lw      $v1, base_address           # ptr = display base address
	add     $v1, $v1, $v0           # ptr += off

	lw      $v0, red           	# color
	sw      $v0, ($v1)              # store pixel
	jr      $ra
    
second_cricle_setpixel:
	lw      $v0, default_width_height          # off = display width

	mul     $v0, $a1, $v0           # off = y * width
	add     $v0, $v0, $a0           # off += x
	sll     $v0, $v0, 2             # convert to offset

	lw      $v1, base_address           # ptr = display base address
	add     $v1, $v1, $v0           # ptr += off

	lw	$v0, black           	# color
	sw	$v0, ($v1)              # store pixel
	jr	$ra

.data
	red:  			.word		0x00FF0000	# red color
	black:  		.word		0x00000000	# black color
	center:			.word 		256		# set center of the circle
	radius:			.word		10		# radius of the circle
	default_width_height:  	.word       	512		# default width and height
	base_address:   	.word       	0x10010000	# base address

